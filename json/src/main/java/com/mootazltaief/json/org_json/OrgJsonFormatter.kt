package com.mootazltaief.json.org_json

import android.content.Context
import com.mootazltaief.json.Utils
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class OrgJsonFormatter(context: Context) {

    private val sample1 = Utils.loadJSONFromAsset(context, "sample1.json")
    private val sample2 = Utils.loadJSONFromAsset(context, "sample2.json")
    private val sample3 = Utils.loadJSONFromAsset(context, "sample3.json")

    private fun formatJson(jsonString: String): String {
        return if (jsonString[0] == '{') {
            JSONObject(jsonString).toString(2)
        } else {
            JSONArray(jsonString).toString(2)
        }
    }

    fun formatSample1(): String = formatJson(sample1)

    fun formatSample2(): String = formatJson(sample2)

    fun formatSample3(): String = formatJson(sample3)


}