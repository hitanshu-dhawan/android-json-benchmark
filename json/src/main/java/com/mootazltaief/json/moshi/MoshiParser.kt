package com.mootazltaief.json.moshi

import android.content.Context
import com.mootazltaief.json.Utils
import com.mootazltaief.json.moshi.models.PhotosMoshi
import com.mootazltaief.json.moshi.models.UserMoshi
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types.newParameterizedType

class MoshiParser(context: Context) {

    private val sample1Json = Utils.loadJSONFromAsset(context, "sample1.json")
    private val sample2Json = Utils.loadJSONFromAsset(context, "sample2.json")
    private val sample3Json = Utils.loadJSONFromAsset(context, "sample3.json")

    val moshi = Moshi.Builder().build()
    private var moshiUserListType = newParameterizedType(List::class.java, UserMoshi::class.java)
    private val moshiUserAdapter: JsonAdapter<List<UserMoshi>> = moshi.adapter(moshiUserListType)
    private val moshiPhotosAdapter: JsonAdapter<PhotosMoshi> = moshi.adapter(PhotosMoshi::class.java)

    private val sample1 = moshiUserAdapter.fromJson(sample1Json)
    private val sample2 = moshiUserAdapter.fromJson(sample2Json)
    private val sample3 = moshiPhotosAdapter.fromJson(sample3Json)


    fun fromJsonSample1() {
        moshiUserAdapter.fromJson(sample1Json)
    }

    fun toJsonSample1() {
        moshiUserAdapter.toJson(sample1)
    }

    fun fromJsonSample2() {
        moshiUserAdapter.fromJson(sample2Json)
    }

    fun toJsonSample2() {
        moshiUserAdapter.toJson(sample2)
    }

    fun fromJsonSample3() {
        moshiPhotosAdapter.fromJson(sample3Json)
    }

    fun toJsonSample3() {
        moshiPhotosAdapter.toJson(sample3)
    }
}