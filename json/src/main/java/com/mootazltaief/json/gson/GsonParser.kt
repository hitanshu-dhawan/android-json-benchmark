package com.mootazltaief.json.gson

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mootazltaief.json.Utils
import com.mootazltaief.json.gson.models.PhotosGson
import com.mootazltaief.json.gson.models.UserGson

class GsonParser(context: Context) {

    private var gson = Gson()

    private val sample1 = Utils.loadJSONFromAsset(context, "sample1.json")
    private val sample2 = Utils.loadJSONFromAsset(context, "sample2.json")
    private val sample3 = Utils.loadJSONFromAsset(context, "sample3.json")

    private val userTypeToken = object : TypeToken<List<UserGson>>() {}.type


    fun fromJsonSample1() {
        gson.fromJson<List<UserGson>>(sample1, userTypeToken)
    }

    fun toJsonSample1() {
        gson.toJson(sample1)
    }

    fun fromJsonSample2() {
        gson.fromJson<List<UserGson>>(sample2, userTypeToken)
    }

    fun toJsonSample2() {
        gson.toJson(sample2)
    }

    fun fromJsonSample3() {
        gson.fromJson(sample3, PhotosGson::class.java)
    }

    fun toJsonSample3() {
        gson.toJson(sample3)
    }
}