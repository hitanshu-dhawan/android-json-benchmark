package com.mootazltaief.benchmark

import androidx.benchmark.junit4.BenchmarkRule
import androidx.benchmark.junit4.measureRepeated
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.mootazltaief.json.gson.GsonParser
import com.mootazltaief.json.jackson.JacksonParser
import com.mootazltaief.json.kotlinx.KotlinParser
import com.mootazltaief.json.moshi.MoshiParser
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class JsonParsersBenchmark {

    @get:Rule
    val benchmarkRule = BenchmarkRule()

    private val appContext = InstrumentationRegistry.getInstrumentation().targetContext

    private val gsonParser = GsonParser(appContext)
    private val moshiParser = MoshiParser(appContext)


    @Test
    fun benchmarkGsonFromJsonSample1() {
        benchmarkRule.measureRepeated {
            gsonParser.fromJsonSample1()
        }
    }

    @Test
    fun benchmarkGsonToJsonSample1() {
        benchmarkRule.measureRepeated {
            gsonParser.toJsonSample1()
        }
    }

    @Test
    fun benchmarkGsonFromJsonSample2() {
        benchmarkRule.measureRepeated {
            gsonParser.fromJsonSample2()
        }
    }

    @Test
    fun benchmarkGsonToJsonSample2() {
        benchmarkRule.measureRepeated {
            gsonParser.toJsonSample2()
        }
    }

    @Test
    fun benchmarkGsonFromJsonSample3() {
        benchmarkRule.measureRepeated {
            gsonParser.fromJsonSample3()
        }
    }

    @Test
    fun benchmarkGsonToJsonSample3() {
        benchmarkRule.measureRepeated {
            gsonParser.toJsonSample3()
        }
    }

    @Test
    fun benchmarkMoshiFromJsonSample1() {
        benchmarkRule.measureRepeated {
            moshiParser.fromJsonSample1()
        }
    }

    @Test
    fun benchmarkMoshiToJsonSample1() {
        benchmarkRule.measureRepeated {
            moshiParser.toJsonSample1()
        }
    }

    @Test
    fun benchmarkMoshiFromJsonSample2() {
        benchmarkRule.measureRepeated {
            moshiParser.fromJsonSample2()
        }
    }

    @Test
    fun benchmarkMoshiToJsonSample2() {
        benchmarkRule.measureRepeated {
            moshiParser.toJsonSample2()
        }
    }

    @Test
    fun benchmarkMoshiFromJsonSample3() {
        benchmarkRule.measureRepeated {
            moshiParser.fromJsonSample3()
        }
    }

    @Test
    fun benchmarkMoshiToJsonSample3() {
        benchmarkRule.measureRepeated {
            moshiParser.toJsonSample3()
        }
    }
}