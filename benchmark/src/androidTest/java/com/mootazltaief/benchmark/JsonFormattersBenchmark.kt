package com.mootazltaief.benchmark

import androidx.benchmark.junit4.BenchmarkRule
import androidx.benchmark.junit4.measureRepeated
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.mootazltaief.json.gson.GsonFormatter
import com.mootazltaief.json.jackson.JacksonFormatter
import com.mootazltaief.json.kotlinx.KotlinFormatter
import com.mootazltaief.json.moshi.MoshiFormatter
import com.mootazltaief.json.org_json.OrgJsonFormatter
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class JsonFormatersBenchmark {

    @get:Rule
    val benchmarkRule = BenchmarkRule()

    private val appContext = InstrumentationRegistry.getInstrumentation().targetContext

    private val gsonFormatter = GsonFormatter(appContext)
    private val moshiFormatter = MoshiFormatter(appContext)
    private val orgJsonFormatter = OrgJsonFormatter(appContext)


    @Test
    fun benchmarkGsonFormatSample1() {
        benchmarkRule.measureRepeated {
            gsonFormatter.formatSample1()
        }
    }

    @Test
    fun benchmarkGsonFormatSample2() {
        benchmarkRule.measureRepeated {
            gsonFormatter.formatSample2()
        }
    }

    @Test
    fun benchmarkGsonFormatSample3() {
        benchmarkRule.measureRepeated {
            gsonFormatter.formatSample3()
        }
    }

    @Test
    fun benchmarkMoshiFormatSample1() {
        benchmarkRule.measureRepeated {
            moshiFormatter.formatSample1()
        }
    }

    @Test
    fun benchmarkMoshiFormatSample2() {
        benchmarkRule.measureRepeated {
            moshiFormatter.formatSample2()
        }
    }

    @Test
    fun benchmarkMoshiFormatSample3() {
        benchmarkRule.measureRepeated {
            moshiFormatter.formatSample3()
        }
    }

    @Test
    fun benchmarkOrgJsonFormatSample1() {
        benchmarkRule.measureRepeated {
            orgJsonFormatter.formatSample1()
        }
    }

    @Test
    fun benchmarkOrgJsonFormatSample2() {
        benchmarkRule.measureRepeated {
            orgJsonFormatter.formatSample2()
        }
    }

    @Test
    fun benchmarkOrgJsonFormatSample3() {
        benchmarkRule.measureRepeated {
            orgJsonFormatter.formatSample3()
        }
    }
}